# Terra Cotta Lofts - Rules & Regulations

*Updated July 2016*

*This update supersedes all previous iterations of the Terra Cotta Lofts Rules and Regulations*

## Table of Contents

1. BICYCLES
2. BULLETIN BOARD
3. COMMON AREAS/ELEMENTS
4. COMMUNITY ROOM
5. COMPLAINTS
6. CONSTRUCTION, ALTERATIONS, REMODELING WITHIN INDIVIDUAL UNITS
7. COURTESY TO NEIGHBORS
8. ELEVATORS
9. EMPLOYEES OF THE ASSOCIATION
10. ENFORCEMENT OF RULES AND REGULATIONS; FINES
11. EQUIPMENT AND SYSTEMS
12. FIRE SAFETY
13. GARBAGE CHUTES AND ROOMS/DISPOSAL OF ITEMS
14. INSURANCE AND LIABILITY
15. KEYS AND LOCKS
16. LEASING A RESIDENTIAL UNIT
17. MOVING
18. REQUIRED CONTACT INFORMATION                                   12
19. PETS                                                           12
20. ROOFTOP DECK                                                   13
21. SALE OF UNITS                                                  15
22. SECURITY                                                       15
23. STORAGE LOCKERS                                                15
24. WINDOWS                                                        16

## 1. Bicycles

a. Bicycles may be stored in the following areas only: (a) the bicycle storage racks in the
basement; (b) within your Unit; or (c) within your Unit's storage locker. Bicycles
stored in any other area are subject to removal at the Owner's expense.

b. All bicycles are stored at the Owner's sole risk and should be secured with a lock.

## 2. Bulletin Board

Bulletin boards are primarily intended for displaying Association communications. All residents
should check the bulletin boards on a regular basis for information regarding meetings and
activities of the Board of Directors, official communications and Managing Agent notices.
Notices, advertisements, propaganda or promotional materials of any kind are not to be
displayed in any common area unless posted on the bulletin board by Board of Directors.
Unit owners may post notices on the back hallway bulletin board. Owner notices may be posted
for a maximum of thirty (30) days and shall not be offensive or abusive.

## 3. COMMON AREAS/ELEMENTS

Common areas and elements are fully defined in the Declaration and By-Laws, which every Unit
Owner receives at or before time of purchase. These documents clearly outline which part of
the property is held in common and which part is personal.
The management, repair, maintenance and improvement of all common areas are the
responsibility of the Condominium Association. All repairs required or performed within the
confines of a Unit are at the Unit Owner's sole expense, except when specifically set out in the
Declaration or by the Board as an item to be maintained at the Association's expense.
2

The Association reserves the right to inspect Units for any changes in equipment affecting the
building's common elements including, but not limited to, vents, plumbing, wiring, doors, or
any conditions not conforming to applicable laws and ordinances. Authorized representatives
(the building engineer, repairmen, etc.) shall be entitled to reasonable access to individual Units
as may be required in connection with maintenance, repairs or replacement of or to either the
building's common elements or limited common elements. There will be reasonable prior
notice given in writing to the Unit Owner, except in cases of an emergency.
Common areas are for the use of all Residents of Terra Cotta Lofts with the following
restrictions:
a.                  Smoking is prohibited in any common area of the building including, but not limited
to the entrances, lobby areas, elevators, hallways, stairwells, basement, and storage
locker rooms.
b.                  Residents shall not tamper with the fire hoses, smoke detectors, emergency lighting
systems or other life-safety equipment in common areas.
c.                  Residents shall not shake out rugs, mops, brooms, dust cloths, or other items on
common areas within the building.
d.                  Children and pets of any kind are not permitted to play in any common area within
the building.
e.                  Except where expressly permitted elsewhere in these Rules and Regulations,
residents shall not attach any fixtures to common areas including, but not limited to,
satellite dishes, wires, antennas, signage or placards of any kind.
f.                  Residents are not permitted to decorate or place any objects on walls, floors, doors
or ceilings in any common area.
g.                  Residents may put seasonal decorations on their door using over-the-door hanger or
other device that does not cause damage to the door. The door must be restored to
its original condition following removal of any decorations at the sole expense of the
resident responsible for its original installation.
h.                  Residents may place a doormat at the foot of their Unit entrance door. They shall
not store any other objects in the hallway; including but not limited to shoes, boots,
umbrellas, empty boxes, bicycles and garbage bags.
i.                  Owners, residents or their guests shall not damage or vandalize common areas or
limited common elements. Unit Owners will be held financially responsible for the
repair of both interior and exterior common elements and limited common elements
damaged by their tenants, family members, employees, agents, delivery persons,
guests or pets.
4.                  COMEV1UNITY ROOM
The following restrictions apply to Community Room functions:
a.                  Only a unit owner may reserve the Community Room  A lessee may reserve the
Community Room through their unit's owner.
b.                  Reservations will only be granted if all assessment payments are current and up to
date and any fines are paid in full.

c.         A reservation request form for the requested event and a $250.00 partially
refundable deposit must be submitted to the Board of Directors at least fourteen (14)
calendar days prior to the event. Reservations are made on a first-come, first-serve
basis. Deposit checks should be made payable to Terra Cotta Lofts.- If no damages,
repairs or clean-up is required following the event, Two Hundred Dollars ($200.00) of
the Two Hundred Fifty Dollar ($250.00) deposit shall be refunded within thirty (30)
business days (Saturdays, Sundays and holidays excluded) following date of
reservation. Immediately following the event, the Community Room must to be
returned to the same condition it was prior to its use. Failure to do so will result in
loss of the $250.00 deposit for all necessary cleaning (including trash removal,
furniture returned to original position, nothing left in refrigerator, etc.). In addition,
the unit owner and/or tenant will lose Community Room use privileges.
d.         Community Room doors shall not be propped open at any time, allowing excessive
noise to emanate into common areas.
e.         Temporary decorations are allowed but must be promptly removed at the end of the
           function. No nails, screws, rivets or other fasteners may be used for any decorative
purposes.
f.         All trash and waste must be removed from the Community Room.
g.         The unit owner is responsible for any damage caused to the community room,
furniture, fixtures or any other common areas by his or her tenants, guests and
family members.
h.         Failure of an owner to reserve the community room for a function will result in a fine
of $250.00 and a charge will be assessed to the unit owner of $75.00 per hour for all
necessary cleaning (including trash removal). In addition, the unit owner and or
tenant may lose Community Room use privileges.
i.         Any damage to the Community Room must be reported to the Property Manager
immediately.
5.         COMPLAINTS
a.         Complaints and notices of violations must be reported in writing to the Board of
Directors or the Management Office using an official Witness Violation Complaint
form. A sample complaint form is attached as Appendix A to this publication or can
be obtained from (a) the Property Manager, (b) the community website, (c) in the
building back service hallway on the bulletin board.
b.         The complainant may be required to appear at a Board hearing to testify about the
complaint. See Section: Enforcement of Rules and Regulations.
6.         CONSTRUCTION, ALTERATIONS, REMODELING INITHIN 1NDNIDUAL
UNITS
a.         Any Unit alterations that require installation of new plumbing, electrical, partitioning
and/or structural elements or other similar work may not proceed unless and until
the Unit Owner receives permission in writing from the Association's Board of
Directors. All such work must be performed by a licensed contractor. The Unit
Owner shall provide proof in copy form of the contractor's license, all permits by
governing authorities having jurisdiction and certificates of insurance for both the
4

contractor and the Unit Owner to the Board for review and approval not less than
thirty (30) business days prior to the start of construction.
b.      Before construction begins, the Unit Owner shall submit to the Board of Directors a
check in the amount of $250.00 payable to the Terra Cotta Lofts Condominium
Association to serve as a damage deposit. This deposit will be credited against
damages of any nature to the building, which may be incurred as a result of
construction. If no damages occur and inspection after construction is satisfactorily
completed, the entire deposit shall be returned to the Unit Owner. The Unit Owner
shall be responsible for all damages incurred by the building in excess of the damage
deposit.
c.      Any work being performed that generates dust or other airborne debris (i.e.
plastering, floor sanding, or demolition) will require the Unit entrance and common
area ventilation systems within the Unit be sealed with plastic tarps so as to minimize
the effect on other residents.
d.      The Unit Owners will be held responsible for the legal removal and disposal of all
construction debris, wood flooring, carpeting, appliances and the like from the
premises at the Owner/contractor's sole expense. Association waste and recycling
containers shall not be used for disposal of debris. If delivery of a dumpster is
required, its placement and days on site must be approved by the Managing Agent, in
writing, prior to its delivery.
e.      Workmen shall clean up after themselves when bringing materials in or out of the
building and when departing. Any failure to comply with regulations that
necessitates that the building staff clean up common areas will result in a charge to
the Unit Owner of $75.00 per hour paid to the Association.
f.      Failure of any contractor to comply with these regulations after having received
notice will result in termination of access to the building until a time as such
problems are resolved.
g.      The Unit Owner shall be liable for any damages to the common elements or to any
other Unit that may occur because of construction, alterations, additions,
improvements, or drilling. The Association may take whatever remedial action it
deems appropriate against the Unit Owner including legal action as necessary to
recover all damages including court and attorney expenses.
h.      The Association reserves the right of entry through the Managing Agent, upon notice
to the Unit Owner to inspect and review the installation of any plumbing, electrical,
and all special and regular equipment and fixtures that require building permits.
i.      Contractors shall not park their vehicles in the designated 30-minute parking area
        located to the front of the building.
j.      Construction and decorating materials may not be stored in any common areas.
k.      The Managing Agent must be advised at least 48 hours in advance so that the
        elevators may be padded throughout construction.
I.      Exterior doors may not be propped open or left unlocked.
m.      Construction and renovations must be completed within ninety (90) calendar days of
        the start date of work, unless an extension is granted in writing by the Board of
        Directors or the Managing Agent.
n.      Construction and remodeling are permitted during the following hours only:
i.      Monday through Friday                                                                 8:00 am to 5:00 pm
ii.     Saturday & Sunday (quiet work only)                                                   9:00 am to 5:00 pm
5

iii.         Elevator use for construction or remodeling purposes prohibited on Saturdays,
Sundays and holidays
7. COURTESY TO NEIGHL4CRS
a.           Owners, residents and their guests shall not permit excessive noise that disturbs
other owners and/or residents to emanate from any Unit, Community Room or
rooftop deck at all times.
b.           Residents shall not prop open unit doors or Community Room doors, allowing
excessive noise to emanate into common areas.
c.           Owners, residents and their guests shall not permit parties or social gatherings to
take place in, or allow guests to congregate in any part of the Common Areas, other
than the roof top deck or second floor Community Room consistent with the
provisions of these Rules and Regulations.
d.           Maintenance or repair work by a contractor, owner or resident that is likely to
disturb other owners or residents must follow the guidelines listed in the Rules and
Regulations under the section titled, Construction, Alterations and Remodeling within
Individual Units.
e.           Quiet hours are Monday — Friday lOpm — 6am and Saturday and Sunday lOpm — 8am.
8. ELEVATORS
In the event of an elevator malfunction, please contact the Property Manager immediately. If
you are trapped in the elevator, immediately call the 24-hour emergency number by using the
call button located in the elevator. Rules regarding the elevator are:
a.           Signs and notices shall not be posted in the elevators or on the exterior elevator
doors, except by the Board of Directors or the Property Manager.
b.           Use of the elevator for delivery of furniture, major appliances, construction materials
and other items require the protection of elevator walls and must be scheduled in
advance through the Property Manager.
c.           City ordinance prohibits smoking in elevators.
9. EMPLOYEES OF THE ASSOCIATION
a.           Employees of the Association carry out day-to-day operations and maintenance of
the building under the supervision of the Property Manager. Association needs are
their first priority.
b.           Only the Property Manager, the President of the Board of Directors (or their duly
appointed representative) may give verbal instructions or work orders to any building
employee.
c.           Owners or residents who hire the Property Manager's maintenance or other
personnel to work in their units do so at their own risk. The Association insurance
does not cover anyone for accidents or injury when working in any Unit (except
under the direction of the Property Manager or the Board of Directors). Neither the
Association nor the Property Manager accepts any responsibility for the quality of
work performed or for the employee's well-being under these circumstances.
6

10.     ENFORCEMENT OF RULES ;.ND REGULATIONS; FINES
a.      Written Violation Notices are issued by the Property Manager, or persons authorized
by the Board of Directors to do so, to the party allegedly committing the violation or
allowing their family members, tenants, guests, invitees or pets to commit a violation
when one of the following occurs:
i.      The Association receives a Witness Violation Complaint. A sample form is
attached as Appendix A to this publication. Witness Violation Complaint forms
may be obtained on the community website as well as from the Property
Manager.
ii.     Should a witness be unwilling to come forward and execute a Witness Violation
Complaint for whatever reason, the witness should contact the Property
Manager.
iii.    The Association receives a letter of complaint which includes (1) the name,
address and telephone number of the complaining witness; (2) the violator's
name and/or address, if known, and (3) the specific details or description of the
violation including the date, time and location where it was alleged to have
occurred.
c.      Notice of Violation (N.O.V.) - A Notice of Violation for the first offense of a particular
rule, regulation or any of the duly recorded covenants, restrictions and bylaws will be
sent by U.S Postal Service certified mail to the owner of record within 30 business
days (holidays, Saturdays and Sundays excluded) of the alleged violation. The Notice
of Violation will include the specifics of the alleged violation along with the amount
of fine imposed. The Property Manager must receive requests for an appeal hearing
within 10 business days (holidays, Saturdays and Sundays excluded) after the receipt
of the Notice of Violation.
d.      Fines for subsequent violations will be compounded, regardless of the particular rule,
regulation or any of the duly recorded covenants, restrictions and bylaws. A Notice of
Violation will be sent by U.S. Postal Service certified mail to the owner of record
within 30 business days (holidays, Saturdays and Sundays excluded) of the alleged
violation. The Notice of Violation will include the specifics of the alleged violation
along with the amount of fine imposed. The Property Manager must receive requests
for an appeal hearing within 10 business days (holidays, Saturdays and Sundays
excluded) after the receipt of the Notice of Violation.
e.      Hearings: The Board of Directors will determine the date, time and place of the
hearing. At that time, the Notice of Violation recipient will have the opportunity to
respond to the complaint. All hearings will proceed with or without the presence of
the accused owner. The decision of the Board of Directors shall be rendered in
writing within 5 business days (holidays, Saturdays and Sundays excluded) after the
appeal hearing and such decision shall be binding upon all parties.
f.      Penalties/Fines
i.      In the event of any violation of the Rules & Regulations, duly recorded
Declarations, Covenants, Restrictions and Bylaws of the association, the Board
of Directors reserves the right to pursue any and all legal remedies to compel
enforcement. Any and all costs and attorney's fees incurred by the association
shall be assessed back to the offending unit owner.
ii.     Rules & Regulations, duly recorded Declarations, Covenants, Restrictions and
Bylaws Violations. Fines for subsequent violations will be compounded,
7

regardless of the particular rule, regulation or any of the duly recorded
covenants, restrictions and bylaws.
1. 1st Offense - $100.00 Fine
2.  2nd Offense - $100 PLUS $250.00 for second offense for a total of $350
3.  3rd Offense - $350 PLUS $500.00 for third offense for a total of $850
4. Other fines as defined in specific sections of these Rules and Regulations (i.e.
rooftop deck and Community Room fines)
11. EQUIPMENT AND SYSTEMS
a.  No unit owner, resident, tenant or their guest may in any way interfere with the
operation of common area systems, including heating, air conditioning, ventilation
and plumbing, nor may use them for their personal purposes.
b.  Only association employees may alter or adjust the settings of any common area
systems or equipment.
c.  Residents shall not prop open unit doors for use of hallway air conditioning or
heating.
d.  Any unit owner, resident, tenant or their guest found tampering or attempting to
tamper with any common area system will immediately be fined $1,000.00.
e.  Any and all costs of repairs incurred by the association to repair a common area
    system which was damaged as a result of tampering by a unit owner, resident, tenant
    or their guest or invitee shall be assessed to the offending unit owner.
12. FIRE SAFETY
a.  Under no circumstances shall any person attempt to remove, dismantle, disconnect
or otherwise disable smoke detectors, emergency lighting systems, fire sprinkler
systems, fire extinguishers, fire hoses, or other life-safety equipment in common
areas or within their units.
b.  No person shall use any common area fire extinguisher or fire hose except in
emergency situations.
c.  Owners, residents, tenants, or guests shall not tamper with or remove smoke
detectors within their unit. If one of these detectors malfunctions, the unit owner
must immediately have it repaired or replaced.
d.  Unit doors should not be propped open or left unattended. This poses a threat to
the fire resistance rating for corridors and allows smoke and flames to reach the
corridor or Unit unimpeded.
e.  All electrical wiring in Units must conform to all applicable electrical codes. Electrical
outlets must not be overloaded.
f.  Except for reasonable quantities of ordinary household products, no hazardous
materials may be stored in a Unit or a storage unit. The following
substances/materials may not be stored or used in the building at any time:
Flammable liquids, explosives, corrosives, biohazards, poisonous, noxious or
radioactive materials or any other substance/material that may compromise the
safety of the building or any of its occupants or users. No firearms or ammunition
8

may be stored in a Unit unless the owner, resident, or tenant has an appropriate
firearms permit or is a commissioned Law Enforcement officer.
g.            Any person tampering or attempting to remove any Common Area fire safety
equipment will immediately be fined $1,000.00. Any insurance increases, fine levied
against the Association by Governmental Authorities will be assessed to the
offending unit owner. Additionally, if anyone is injured or property damaged as a
result of such tampering, the offender may be subject to severe civil or criminal
penalties and or prosecution.
h.            Any and all costs of repairs incurred by the association to repair a Common Area fire
system which was damaged as a result of tampering by a unit owner, resident, tenant
or their guest or invitee shall be assessed to the offending unit owner.
i.            Any owner, resident, tenant, or their guest causing the sounding of a false fire alarm
will immediately be fined $1000.00. Any insurance increases, and/or fines levied
against the Association by Governmental Authorities will be assessed to the
offending unit owner. Additionally, if anyone is injured or property damaged as a
result of such tampering, the offender may be subject to severe civil or criminal
penalties and or prosecution.
13. Cf-'.RUAGE: CHUTES AND ROOMS/DISPOSAL OF ITEMS
There is a trash chute access door on each floor of the building. The building has a
trash/dumpster room on the first floor directly below the chute. The building has a recycling
area          located in the back service hallway on the first floor.
a.            Use of garbage chute before 6:00 A.M. and after 10:00 P.M. Monday - Friday is
prohibited. Also use of garbage chute before 8:00 A.M. and after 10:00 P.M. Saturday
and Sunday is prohibited. This is in consideration of Units located around the chute.
b.            All garbage must be securely bagged in leak-proof plastic bags before being
transported from Unit to trash chute.
c.            Garbage chutes shall not be used to dispose of items (boxes, rugs, brooms or any
other large or bulky items) that may cause the chute to become blocked.
d.            Owners, residents, tenants, or their guests are prohibited from disposing of glass
down the garbage chute. Instead, glass should be brought down to the recycling area
and placed in the appropriate container.
e.            Owners, residents, tenants, or their guests are prohibited from disposing of grease
down the chute. Instead, grease should be placed in a tightly sealed container and
brought directly to the first floor trash room and placed in the dumpster.
f.            Owners, residents, tenants, or their guests are prohibited from disposing of cat litter,
soiled diapers and other similarly noxious items down the garbage chute. Instead,
these type items should be placed in a tightly sealed bag and brought directly to the
first floor trash room and placed in the dumpster.
g.            Owners, residents, tenants, or their guests are prohibited from disposing of burning,
toxic, flammable, or other dangerous materials in the first floor garbage room or
down the garbage chute.
h.            Owners, residents, tenants, or their guests are prohibited from disposing of any
appliances, furniture, construction debris, carpeting, wood flooring or other personal
property in the first floor garbage room, in the back service hallway or in any other
9

common area of the building. All such items must be removed from the premises at
Owner's expense.
i.              Any and all costs incurred by the Association for the disposal of items left by owners,
residents, tenants, or their guests in a common area shall be assessed to the
offending unit owner.
j.              Nothing is to be left in any of the small trash chute rooms located on floors 2 through
12.
k.              All cardboard boxes must be broken down before being placed in the appropriately
marked recycling bin. Failure to break down cardboard boxes, or any cardboard
before it is placed in the recycling bin will result in a $50.00 per box fine being
assessed to the Unit Owner.
I.              Disposal of Christmas trees is the responsibility of the owner. They may not be left in
the back hallway, nor left on St. Charles Street.
14.             INSURANCE ARID LIABILITY
a.              Property Insurance - The Association provides a master Policy of property insurance
for insured losses to the Common Elements, Limited Common Elements, and the
Units. Coverage of the units under the Master Policy includes replacement of items
attached to the building, which are owned and used exclusively by individual unit
owners, such as fixtures, carpeting or other flooring, plumbing and lighting fixtures,
built-in appliances and cabinets; but only to the extent, type, and quality of such
items as installed when the unit was originally sold to the first Owner.
b.              Each unit owner shall be responsible for a Personal Insurance Policy providing
coverage for the Owner's personal property, personal liability, and for damages to
the unit which are below the Master Policy deductible. This policy should include
Special Form and Earthquake coverage.
c.              Each unit owner is responsible for the deductible on his/her own Personal Policy,
regardless of the cause of loss.
d.              To obtain a copy of the Association's Master Insurance Policy, please contact the
Property Manager.
15.             KEYS AND LOCKS
Mailbox keys, door keys and storage locker locks are the responsibility of each owner. Fobs are
also the responsibility of the owner.
16. LEASING A RESIDENTIAL UNIT
a.              A residential unit owner may lease his or her unit (but not less than his or her entire
unit) at any time and from time to time provided that:
i.              No residential unit or portion of a unit may be leased for transient or hotel
purposes (i.e. AirBnB) or for a term of less than one (1) year.
ii.             No residential unit may be leased without a written lease that complies with
the recorded Declaration of Condominium and of Easements, Restrictions,
Covenants and By-Laws for the Terra Cotta Lofts Condominium.
10

iii.  A copy of such lease shall be furnished to the Board of Directors within ten (10)
calendar days after execution. Failure to do so will result in a fine of $100.
iv.   A copy of a signed acknowledgement (executed by both Lessor and Lessee)
shall be furnished to the Board of Directors within ten (10) calendar days after
execution of lease by the Unit Owner indicating that the lessee has received
and read a copy of the current rules and regulations and that lessee
understands and agrees to abide by any and all rules and regulations imposed
by the Board of Directors.
v.    The rights of any lessee of the unit shall be subject to, and each such lessee
shall be bound by the covenants, conditions and restrictions set forth in the
Declaration, By-Laws and Rules and Regulations and a default there under shall
constitute a default under the lease or sublease; provided, however, that the
foregoing shall not impose any direct liability on any lessee or sub lessee of a
unit to pay any common expense assessments or special assessments on behalf
of the owner of that unit.
b.    All moves must be scheduled with the Property Manager.
a.    When any unit owner leases a unit, a partially-refundable move-in/move-out fee of
four hundred dollars ($400.00) shall be collected not less than seven (7) business
days (Saturdays, Sundays, and holidays excluded) prior to move-in/move-out. Said
deposit will be place with the property manager until the move is complete. If no
damages are noted and repairs necessary as a result of the move in and/or move out,
two hundred ($200.00) of the four hundred ($400.00) shall be refunded within thirty
(30) business days (Saturdays, Sundays, and Holidays excluded) of the completed
move.
c.    Owners must notify the Association via the Property Manager not less than seven (7)
      business days (Saturdays, Sundays and holidays excluded) prior to any tenant moving
      in or out of their Unit. Failure to do so will result in a fine of $100.
d.    Refer to Moving (sections g-q) for move-in/move-out rules.
17.   MOVING
b.    When any owner moves in/moves out, a partially-refundable move-in/move-out fee
of four hundred dollars ($400.00) shall be collected not less than seven (7) business
days (Saturdays, Sundays, and holidays excluded) prior to move-in/move-out. Said
deposit will be place with the property manager until the move is complete. If no
damages are noted and repairs necessary as a result of the move in and/or move out,
two hundred ($200.00) of the four hundred ($400.00) shall be refunded within thirty
(30) business days (Saturdays, Sundays, and Holidays excluded) of the completed
move.
c.    Owners must notify the Association via the Property Manager not less than seven (7)
      business days (Saturdays, Sundays and holidays excluded) prior to any person moving
in or out of their Unit.
d.    All moves must be scheduled with the Property Manager.
e.    Prior to any move, a condition assessment of the lobby and the path to the unit will
be completed and pads hung in the North elevator.
f.    Following a move, a condition assessment of the lobby and path to the unit will be
completed to identify any damages caused as a result of the move.
11

g.                If repair cost of damages exceeds the refundable deposit portion of the partially
                  refundable deposit, the moving owner will be billed for the excess cost of all repairs.
h.                Only the North elevator may be used for moves.
i.                Use of the North elevator for a move must be scheduled with the Property Manager
at least seven (7) days in advance.
j.                The North elevator may be reserved by a unit owner for up to two (2) consecutive
days.
k.                Reservation of the North elevator is made on a first-come, first-serve basis.
I.                The South elevator shall not be used for moves unless North elevator is disabled and
permission has been granted by the Property Manager.
m.                Elevator use will not be granted to an owner for a move until all current and
outstanding assessments pertaining to their unit are paid in full.
n.                Only the rear building entrance (on St. Charles Street) may be used for moves. Under
no circumstances may the front building entrance (on Locust Street) be used for
moves.
o.                To maintain building security during moves, the owner or tenant moving must have
a person positioned at the building entranceway being used. Entrance doors shall
not be left open and unattended.
p.                Moving vans and trucks cannot be parked as to limit entrance to a driveway or
parking spaces.
q.                        If dollies are used, all dollies must have rubber wheels to prevent any damage to
floors and stairs.
r.                        Moving in or out of a unit may take place only during the following hours:
i.                Monday thru Friday                                                                        9:00 am to 5:00 pm
ii.               Saturday                                                                                  8:00 am to 3:00 pm
iii.              Sunday and Holidays                                                                       8:00 am to 3:00 pm
18.               REQURED CONTACT VkIFORiArtiATION
In addition to any owner information required in other sections of the Rules and Regulations, all
owners shall provide the Management Company with current email, mailing address, and
daytime and evening telephone numbers.
Owner must supply the Management Company with email and phone number(s) of all
individuals living in their unit.
19. PETS
a.                Owners and or their tenants may house no more than two (2) pets and a reasonable
                  number of fish, small birds, or caged animals normally maintained in households.
b.                No pets shall be maintained for breeding or other commercial purposes.
c.                All pets must be carried, kept on a short leash, or in a carrier when in any common
area in the building.
d.                Pets are prohibited on the roof top deck, as well as any other premises designated by
the Board from time to time.
e.                Pets may not perform elimination functions in any area on the Association premises.
12

Pet elimination functions are prohibited within twenty (20) feet of any Association
building entrance.
g.        Any accidental elimination function by a pet in the building or within twenty (20) feet
of the building must be cleaned up immediately. The cost of any additional cleaning
and deodorizing due to pet elimination (including, but not limited to the cleaning of
carpet) shall be charged to the pet owner (or unit owner).
h.        Pet owners are fully responsible for any property damage or personal injury caused
by their pets.
i.        Pets that cause injury to any person or other animal or Association property;
continuously disturb other owners (or their tenants); or cause damage to an owner's
or the Association's property may be permanently removed from the property by
direction of the Board of Directors. If a pet owner or his agent violates repetitively or
allows the pet to violate repetitively these rules, then the pet may be permanently
removed from the unit upon direction of the Board of Directors.
20.       ROOFTOP DECK
a.        Only owners (and/or their tenants) and guests accompanied by an owner (or tenant)
are permitted to use the rooftop deck.
b.        Owners are not only responsible for their own actions at all times while on the
rooftop deck but also for those of their tenants, guests and family members.
c.        No person, including owners, tenants, guests, or family members (other than those
approved by the Board of Directors or the Property Manager — e.g. maintenance
workers) may walk on any area of the roof surface other than the rooftop deck and
designated maintenance walkway (located on the North side of the PH elevator
lobby).  In view of the severity and danger of any such activity, the owner upon first
infraction of this rule, will incur a fine of $1,000.00 and rooftop access will be lost
for up to one (1) year.
d.        Nothing may be thrown or intentionally dropped over the edge of the deck or the
          edge of the roof. In view of the severity and danger of any such activity, the owner,
          upon first infraction of this rule will incur a fine of at least $1,000.00 and rooftop
          usage privileges will be lost for up to one (1) year.
e.        An adult must accompany children at all times.
f.        Personal grills are not allowed. The Association reserves the right to provide a gas
grill to be placed on the roof deck for shared use. Under no circumstances may an
owner, tenant or their guest move the gas grill from its approved location. The gas
grill may only be operated according to the manufacturer's recommendations. In
view of the severity and danger of any of the aforementioned activities, the owner
upon first infraction of this rule will incur a fine of $1,000.00 and rooftop privileges
will be lost for up to one (1) year.
g.        No pets are allowed on the rooftop deck or roof surface at any time.
h.        No fireworks are allowed on the rooftop deck.
All garbage and waste is to be placed in the provided containers. In the event that
the provided containers are full, all excess waste must be removed from the rooftop
deck area and disposed of via the trash chute according to the rules and regulations
that govern the use of the trash chute.
j.        Sleeping all night on the rooftop deck is not permitted.
13

k.  Proper attire is required at all times while on the rooftop deck. No nude or topless
sunbathing is permitted at any time.
I.  No sexual activity is permitted at any time.
m.  Sound systems may not be operated at loud volume.
n.       Rooftop deck is available for functions and social events at the following times:
i.  Sunday through Thursday                                                                11:00 am to 9:00 pm
ii. Friday and Saturday                                                                    11:00 am to 12:00 am (midnight)
o.  The following restrictions apply to rooftop deck functions:
i.       Functions are limited to forty (40) people. A reservation must be made if there
are ten (10) or more people expected at the event.
ii. Reserving the rooftop deck allows exclusive use of the grill and furniture, but
does not prohibit other residents from being on the deck.
iii.                                                                                  A reservation request form for the requested event and a $250.00 partially
refundable deposit must be submitted to the Board of Directors at least
fourteen (14) calendar days prior to the event. Reservations are made on a
first-come, first-serve basis. Deposit checks should be made payable to Terra
Cotta Lofts.- If no damages, repairs or cleanup is required following the event,
two hundred dollars ($200.00) of the two hundred fifty dollar ($250.00) deposit
shall be refunded within thirty (30) business days (Saturdays, Sundays and
holidays excluded) following date of reservation. Immediately following the
event, the rooftop deck must to be returned to the same condition it was prior
to its use. Failure to do so will result in loss of the $250.00 deposit for all
necessary cleaning (including trash removal, furniture returned to original
position, etc.). In addition, the unit owner and/or tenant will lose rooftop deck
use privileges for up to three months.
iv. Failure of an owner to reserve the rooftop deck for a function with ten (10) or
more people will result in a fine of $100.00. In addition, the unit owner and or
tenant may lose rooftop deck privileges for up to three (3) months.
v.  Reservations will only be granted if all assessment payments are current and up
to date and any fines are paid in full.
vi. Only a unit owner may reserve the rooftop deck. A lessee may reserve the
rooftop deck through their unit's owner.
vii.                                                                                  Temporary decorations are allowed but must be promptly removed at the end
of the function. No nails, screws, rivets or other fasteners may be used on any
common area for any decorative purposes whatsoever.
viii.                                                                                 All trash and waste must be removed from the rooftop deck (including the
provided trash containers) by the party reserving the rooftop deck.
ix. Immediately following the event, the rooftop deck must to be returned to the
same condition it was prior to its use. Failure to do so will result in loss of the
$250.00 deposit and a charge will be assessed to the unit owner of $75.00 per
hour for all necessary cleaning (including trash removal). In addition, the unit
owner and or tenant may lose rooftop deck usage privileges for up to three (3)
months.
x.  The unit owner is responsible for any damage caused to the rooftop deck, roof
surface, furniture, fixtures or any other common areas by his or her tenants,
guests and family members.
14

xi.    Any damage to the rooftop deck or any furniture or fixtures must be reported
to the Property Manager immediately.
21.    SALE OF UNITS
a.               Unit owners selling their unit must submit to the Board of Directors via the Property
                 Manager the following at least thirty (30) calendar days prior to closing:
       i.        A copy of the fully executed sales contract (including any and all addendums),
                 as provided by either the seller or buyer
b.               All unpaid assessments must be paid in full prior to the closing of the sale.
c.               The selling unit owner is responsible for transferring his or her copies of the
                 Declarations, By-Laws and Rules and Regulations to the purchaser.
d.               The Association does not inspect units at the time of sale, and makes no warranty
                 whatsoever regarding compliance of the unit with building codes or Association
       rules.
22.    SECURITY
a.     It is necessary that all owners, tenants, guests and or their family members strictly
adhere to all rules so as not to jeopardize the safety of any Terra Cotta Lofts owner,
resident, guest and or their family members.
b.     Entrance doors to the property shall never be left unlocked or propped open. If an
owner or tenant notices an entrance door in such a state, he and/or she should
immediately close and make sure the door is locked and notify the Property Manager
of the incident.
c.     Do not allow unauthorized access, i.e. allowing others to use your access card,
allowing others you do not know to follow you into the building without a fob
rtailgating'), walking away from the door before it closes and locks which may allow
unauthorized access.
d.     No solicitation is allowed in the building by any person, including solicitations by unit
owners and tenants on behalf of others. An owner or tenant who identifies someone
soliciting in the building during business hours should notify the Property Manager
immediately and the police if the solicitation is taking place after hours.
e.     When hosting a social event or open house, owners, tenants, and/or their duly
designated agents (i.e. real estate agent, valet parking attendants) shall meet anyone
they do not personally know at the entrance to the building. Anyone not personally
known by the owner, tenant or duly designated agent shall not be allowed to gain
entrance to the building
23.    STOnAC7 LOCKERS
a.     Each unit is allocated one (1) storage locker. Storage lockers are located in the
       basement of the building.
b.     All storage lockers are identified with a number.
c.     Any unit owner or tenant using a storage locker other than the one designated may
be opened and the contents may become the property of the Association and or
destroyed at the full cost of the property owner.
15

d.     Storage of items outside the lockers is not permitted.
e.     No flammable liquids or hazardous materials may be stored in a storage locker or
room. The following substances/materials may not be stored or used in the building
at any time: Flammable liquids, explosive, corrosive, biohazards, poisonous, noxious
or radioactive materials or any other substance/material that may compromise the
safety of the building or any of its occupants or users. No firearms or ammunition
may be stored in a storage locker unless the owner, resident, or tenant has an
appropriate firearms permit or is a commissioned Law Enforcement officer.
f.     Small amounts of non-flammable household paint may be stored in storage lockers.
g.     All storage lockers must be secured by a lock at all times. If a storage locker is not
properly secured by a lock, the Association may at its discretion purchase and install
a lock on a locker and the full cost of the lock will be assessed to the unit owner of
record.
h.     The Association is not responsible for damage to or loss of any item stored in a
storage locker or room.
24. WINDOWS
a.     Unit owners are prohibited from replacing window frames but as limited common
elements of their unit, are responsible for broken glass and repairs.
b.     Posters, decals, banners, advertisements, and signs shall not be displayed in unit
windows without permission of the Board.
c.     No owner and/or tenant may cover the windows in their unit with non-traditional
window coverings, i.e. newspaper, magazines, sheet, at any time.
d.     No items may be placed on, or stored, on exterior window ledges or sills at any time.
END OF TERRA COTTA LOFTS RULES AND REGULATIONS
UPDATED July 19, 2016
16
